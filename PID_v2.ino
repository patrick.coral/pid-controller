#include <QuickPID.h>
#include <AVR_PWM.h>

AVR_PWM* PWM_Instance;

#define MAX_RPM 2200
#define MIN_RPM 500
#define MIN_PWM 20
#define START_PWM 25

#define PWM_FREQ 6000

// don't use pins attached to Timer0 (5 + 6 on Nano)
#define PWM_PIN 9
#define POT_PIN A0
#define INT_PIN 2

volatile int pulse_count = 0;
volatile int prev_time = 0;
volatile int delta_time = 0;

float Kp = 0.5, Ki = 0.5, Kd = 0.05;
float input_RPM, output_RPM, target_RPM;
QuickPID myPID(&input_RPM, &output_RPM, &target_RPM, Kp, Ki, Kd, QuickPID::Action::direct);

unsigned long h, m, s, ms, timestamp, now, start;
char buffer[16];

void setup() {
  pinMode(POT_PIN, INPUT);
  pinMode(INT_PIN, INPUT);

  Serial.begin(9600);

  attachInterrupt(digitalPinToInterrupt(INT_PIN), count_pulse, RISING);

  prev_time = millis();

  // Startup sequence
  // Run @ 25% for 1s then at min. dutycycle
  Serial.println("\n\n\n\nSetup...");
  PWM_Instance = new AVR_PWM(PWM_PIN, PWM_FREQ, 70);
  PWM_Instance->setPWM(PWM_PIN, PWM_FREQ, 0);
  delay(1000);
  Serial.println("Done.");

  myPID.SetOutputLimits(MIN_RPM, MAX_RPM);
  myPID.SetMode(QuickPID::Control::timer);
}

bool timeset = false;

void loop() {
  // Set initial time
  while (!timeset) {
    Serial.print("H: ");
    Serial.flush();
    while (!Serial.available());
    h = (unsigned long)Serial.parseInt();
    Serial.read();
    delay(300);
    Serial.print("M: ");
    while (!Serial.available());
    m = (unsigned long)Serial.parseInt();
    Serial.read();
    delay(300);
    Serial.print("S: ");
    Serial.flush();
    while (!Serial.available());
    s = (unsigned long)Serial.parseInt();
    delay(300);

    Serial.print("H:");
    Serial.println(h);
    Serial.print("M:");
    Serial.println(m);
    Serial.print("S:");
    Serial.println(s);
    Serial.flush();

    timeset = true;
    timestamp = h * 60 * 60 * 1000 + m * 60 * 1000 + s * 1000 - millis();
  }

  now = timestamp + millis();
  h = (now / 1000 / 60 / 60) % 24;
  m = (now / 1000 / 60) % 60;
  s = (now / 1000) % 60;

  input_RPM = constrain(60.0 / (2.0 * (delta_time / 1000.0)), 0, 2200);
  if (input_RPM < 100) {
    PWM_Instance->setPWM_manual(PWM_PIN, 0);
  }

  Serial.print(h);
  Serial.print(":");
  Serial.print(m);
  Serial.print(":");
  Serial.print(s);
  Serial.print(".");
  Serial.print(now % 1000);
  Serial.print("; ");

  Serial.print("Actual_RPM:");
  Serial.print((int)input_RPM);
  Serial.print("; ");

  int pot = analogRead(A0);
  target_RPM = map(pot, 0, 1023, MIN_RPM, MAX_RPM);
  target_RPM = (double)((int)target_RPM / 10) * 10;

  Serial.print("Target_RPM:");
  Serial.print((int)target_RPM);
  Serial.print("; ");


  Serial.print("Output_RPM:");
  Serial.print((int)output_RPM);
  Serial.println("; ");

  myPID.Compute();

  float new_level = RPM_to_PWM(output_RPM) * PWM_Instance->getPWMPeriod() / 100.0f;
  PWM_Instance->setPWM_manual(PWM_PIN, new_level);
}

void count_pulse() {
  delta_time = millis() - prev_time;
  prev_time = millis();
}

int RPM_to_PWM(int RPM) {
  // Output is inverted (75 -> 25)
  return map(RPM, MIN_RPM, MAX_RPM, 75, 1);
}
